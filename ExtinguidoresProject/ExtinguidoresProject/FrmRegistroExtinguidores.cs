﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExtinguidoresProject
{
    public partial class FrmRegistroExtinguidores : Form
    {
        DataSet dsExtinguidor;
        BindingSource bsExtinguidor;

        public DataSet DsExtinguidor
        {
            get
            {
                return dsExtinguidor;
            }

            set
            {
                dsExtinguidor = value;
            }
        }

        public FrmRegistroExtinguidores()
        {
            InitializeComponent();
            bsExtinguidor = new BindingSource();
        }

        private void FrmRegistroExtinguidores_Load(object sender, EventArgs e)
        {
            bsExtinguidor.DataSource = DsExtinguidor;
            bsExtinguidor.DataMember = DsExtinguidor.Tables["Extinguidores"].TableName;
            dgvExtinguidor.DataSource = bsExtinguidor;
            dgvExtinguidor.AutoGenerateColumns = true;
        }

        private void btnDelate_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvExtinguidor.SelectedRows;

            if(rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, Debe seleccionar una fila para eliminar",
                    "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow dRow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this,"Realmente desea eliminar el registro?", 
                "Mensaje del sitema",MessageBoxButtons.YesNo, MessageBoxIcon.Question );
            if(result == DialogResult.Yes)
            {
                dsExtinguidor.Tables["Extinguidores"].Rows.Remove(dRow);
                MessageBox.Show(this, "Eliminado sastifactoriamente",
                    "Mensaje del sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void txtfinder_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsExtinguidor.Filter = string.Format("Categoria like '*{0}*' or Marca like '*{0}*' or Tipo de extinguidores like '*{0}*' ", txtfinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmExtinguidores fe = new FrmExtinguidores();
            fe.DtExtinguidor = DsExtinguidor.Tables["Extinguidores"];
            fe.DsExtinguidor = DsExtinguidor;
            fe.ShowDialog();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvExtinguidor.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmExtinguidores fp = new FrmExtinguidores();
            fp.DtExtinguidor = DsExtinguidor.Tables["Producto"];
            fp.DsExtinguidor = DsExtinguidor;
            fp.DrExtinguidores = drow;
            fp.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            reporte report = new reporte();
            report.DsExtinguidor = DsExtinguidor;
            report.ShowDialog();
        }
    }
}
