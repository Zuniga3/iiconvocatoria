﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExtinguidoresProject
{
    public partial class reporte : Form
    {
        DataRow drExtinguidior;
        DataSet dsExtinguidor;

        public reporte()
        {
            InitializeComponent();
        }

        public DataRow DrExtinguidior
        {
            get
            {
                return drExtinguidior;
            }

            set
            {
                drExtinguidior = value;
            }
        }

        public DataSet DsExtinguidor
        {
            get
            {
                return dsExtinguidor;
            }

            set
            {
                dsExtinguidor = value;
            }
        }

        private void reporte_Extintorcs_Load(object sender, EventArgs e)
        {
           
            DataTable dtExtinguidores = dsExtinguidor.Tables["Extinguidores"];
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ExtinguidoresProject.ReporteExtintor.rdlc";
            ReportDataSource rds1 = new ReportDataSource("DataSet1", dsExtinguidor.Tables["Extinguidores"]);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds1);

            this.reportViewer1.RefreshReport();
        }
    }
}
