﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExtinguidoresProject
{
    public partial class FrmExtinguidores : Form
    {
        DataTable dtExtinguidor;
        DataSet dsExtinguidor;
        DataRow drExtinguidores;
        BindingSource bsExtinguidores;

        public DataTable DtExtinguidor
        {
            get
            {
                return dtExtinguidor;
            }

            set
            {
                dtExtinguidor = value;
            }
        }

        public DataSet DsExtinguidor
        {
            get
            {
                return dsExtinguidor;
            }

            set
            {
                dsExtinguidor = value;
            }
        }

        public DataRow DrExtinguidores
        {
           
            set
            {
                drExtinguidores = value;
                cmbmarca.Text = drExtinguidores["Marca"].ToString();
                cmbcap.Text = drExtinguidores["Capacidad"].ToString();
                cmbtipo.Text = drExtinguidores["Tipo de extinguidor"].ToString();
                cmbunidad.Text = drExtinguidores["Unidad de medida"].ToString();
                cmbcat.Text = drExtinguidores["Categoria"].ToString();
                txtcantidad.Text = drExtinguidores["Cantidad"].ToString();
            }
        }

        public FrmExtinguidores()
        {
            InitializeComponent();
            bsExtinguidores = new BindingSource();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string categoria, marca, tipo, unidad;
            int capacidad, cantidad;

            categoria = cmbcat.Text;
            marca = cmbmarca.Text;
            tipo = cmbtipo.Text;
            unidad = cmbunidad.Text;
            capacidad = int.Parse(cmbcap.Text);
            cantidad = int.Parse(txtcantidad.Text);

            if (drExtinguidores != null)
            {
                DataRow drNew = dtExtinguidor.NewRow();

                int index = dtExtinguidor.Rows.IndexOf(drExtinguidores);
                drNew["Id"] = drExtinguidores["Id"];
                drNew["Marca"] = marca;
                drNew["Capacidad"] = capacidad;
                drNew["Tipo de Extinguidor"] = tipo;
                drNew["Unidad de medida"] = unidad;
                drNew["Categoria"] = categoria;
                drNew["cantidad"] = cantidad;
               

                dtExtinguidor.Rows.RemoveAt(index);
                dtExtinguidor.Rows.InsertAt(drNew, index);

            }
            else
            {
                dtExtinguidor.Rows.Add(dtExtinguidor.Rows.Count + 1,marca,capacidad, tipo, unidad, cantidad, categoria);
            }

            Dispose();
        }

        private void FrmExtinguidores_Load(object sender, EventArgs e)
        {
            bsExtinguidores.DataSource = DsExtinguidor;
            bsExtinguidores.DataSource = DsExtinguidor.Tables["Extinguidores"].TableName;
        }
    }
}
