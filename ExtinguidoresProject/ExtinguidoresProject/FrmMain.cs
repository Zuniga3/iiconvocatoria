﻿using ExtinguidoresProject.Entities;
using ExtinguidoresProject.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExtinguidoresProject
{
    public partial class FrmMain : Form
    {
        DataTable dtSistema;
        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            dtSistema = DsSistema.Tables["Extinguidores"];
            ExtinguidorModel.populate();

            foreach(Extinguidor p in ExtinguidorModel.getAll())
            {
                dtSistema.Rows.Add(p.Id, p.Marca, p.Capacidad, p.Tipo_de_extinguidor, p.Unidad_de_medida, p.Cantidad, p.Category);
            }

        }

        private void catalogoDeExtinguidoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRegistroExtinguidores fre = new FrmRegistroExtinguidores();
            fre.MdiParent = this;
            fre.DsExtinguidor = DsSistema;
            fre.Show();
        }
    }
}
