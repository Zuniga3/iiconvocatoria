﻿using ExtinguidoresProject.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtinguidoresProject.Model
{
    class ExtinguidorModel
    {
        private static List<Extinguidor> lstextinguidor = new List<Extinguidor>();

        public static List<Extinguidor> getAll()
        {
            return lstextinguidor;
        }

        public static void populate()
        {
            Extinguidor[] ArrayExtinguidor =
            {
                new Extinguidor (1, "AMEREX", 5, "CO2","Litros", 10,Extinguidor.categoria.AMERICANO)
            };

            lstextinguidor = ArrayExtinguidor.ToList();
        }
    }
}
