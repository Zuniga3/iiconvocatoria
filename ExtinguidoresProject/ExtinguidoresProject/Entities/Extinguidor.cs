﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtinguidoresProject.Entities
{
    class Extinguidor
    {
        private int id;
        private string marca;
        private int capacidad;
        private  string tipo_de_extinguidor;
        private string unidad_de_medida;
        private int cantidad;
        private categoria category;

        public Extinguidor(int id, string marca, int capacidad, string tipo_extinguidor, string unidad_medida, int cantidad, categoria category)
        {
            this.id = id;
            this.marca = marca;
            this.capacidad = capacidad;
            this.tipo_de_extinguidor = tipo_extinguidor;
            this.unidad_de_medida = unidad_medida;
            this.cantidad = cantidad;
            this.Category = category;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        public int Capacidad
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        public string Tipo_de_extinguidor
        {
            get
            {
                return tipo_de_extinguidor;
            }

            set
            {
                tipo_de_extinguidor = value;
            }
        }

        public string Unidad_de_medida
        {
            get
            {
                return unidad_de_medida;
            }

            set
            {
                unidad_de_medida = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        internal categoria Category
        {
            get
            {
                return category;
            }

            set
            {
                category = value;
            }
        }

        public enum categoria
        {
            AMERICANO, ASIATICO, EUROPEO
        }


     }
}
